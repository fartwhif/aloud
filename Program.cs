﻿using NAudio.Wave;

using Newtonsoft.Json;

Aloud.Run();
internal class Aloud
{
    enum seat
    {
        attended,
        unattended
    };
    enum sound
    {
        crash,
        suck
    };
    static int eventsLen = 0;
     const int attendedCrashCount = 1;
    public static void Run()
    {
        playAlertSound(sound.suck);
        GetEventsChanged(false);
        Console.WriteLine($"Aloud started @ {DateTime.Now}, {eventsLen:N0} events in log.");
        processUserInput(ConsoleKey.U);
        while (true)
        {
            getUserInput();
            Thread.Sleep(1000);
            GetEventsChanged(true);
        }
    }
    class Pagination
    {
        public int Page { get; set; }
        public int Current { get; set; }
        public int Count { get; set; }
        public int pageCount { get; set; }
        public bool prevPage { get; set; }
        public bool nextPage { get; set; }
    }
    class EventQueryResponse
    {
        public Pagination Pagination { get; set; }
    }
    static seat seatStatus = seat.unattended;
    static void GetEventsChanged(bool active)
    {
        HttpClient client = new HttpClient();
        string jsonStringEvents = client.GetStringAsync("http://myzmserver/zm/api/events.json").Result;
        EventQueryResponse? eventQueryResponse = JsonConvert.DeserializeObject<EventQueryResponse>(jsonStringEvents ?? "{}");
        int eventCount = eventQueryResponse?.Pagination?.Count ?? 0;
        if (eventsLen != eventCount)
        {
            eventsLen = eventCount;
            if (active)
            {
                switch (seatStatus)
                {
                    case seat.unattended:
                        unatendedCrash();
                        break;
                    case seat.attended:
                        attendedCrash();
                        break;
                    default:
                        break;
                }
            }
        }
    }
    static void getUserInput()
    {
        if (Console.KeyAvailable)
        {
            ConsoleKeyInfo? key = null;
            while (Console.KeyAvailable)
            {
                key = Console.ReadKey(true);
            }
            if (key.HasValue)
            {
                var _key = key.Value.Key;
                processUserInput(_key);
            }
        }
    }
    static void processUserInput(ConsoleKey key)
    {
        if (key.ToString().ToUpper() == "U")
        {
            if (seatStatus == seat.attended)
            {
                seatStatus = seat.unattended;
                Console.WriteLine($"seat is now {seatStatus} @ {DateTime.Now}");
            }
            else if (seatStatus == seat.unattended)
            {
                Console.WriteLine($"seat is already {seatStatus} @ {DateTime.Now}");
            }
        }
        else if (key.ToString().ToUpper() == "A")
        {
            if (seatStatus == seat.unattended)
            {
                seatStatus = seat.attended;
                Console.WriteLine($"seat is now {seatStatus} @ {DateTime.Now}");
            }
            else if (seatStatus == seat.attended)
            {
                Console.WriteLine($"seat is already {seatStatus} @ {DateTime.Now}");
            }
        }
    }
    static void attendedCrash()
    {
        Console.WriteLine($"Event count changed to {eventsLen} @ {DateTime.Now}");
        int count = 0;
        while (count < attendedCrashCount)
        {
            playAlertSound(sound.crash);
            count++;
        }
        GetEventsChanged(false);
    }
    static void unatendedCrash()
    {
        Console.WriteLine($"Event count changed to {eventsLen} @ {DateTime.Now}, strike any key for silence.");
        while (!Console.KeyAvailable)
        {
            playAlertSound(sound.crash);
        }
        _ = Console.ReadKey(true);
        GetEventsChanged(false);
        Console.WriteLine($"Silenced @ {DateTime.Now}  {eventsLen} events.");
    }
    static void playAlertSound(sound sound)
    {

        Dictionary<sound, string> wavFilePaths = new Dictionary<sound, string>();
        wavFilePaths.Add(sound.crash, ".\\SampleData\\Drums\\crash-trimmed.wav");
        wavFilePaths.Add(sound.suck, ".\\SampleData\\particle_suck2.wav");

        string wavFile = wavFilePaths[sound];

        using (var waveOut = new WaveOutEvent())
        using (var wavReader = new WaveFileReader(wavFile))
        {
            waveOut.Init(wavReader);
            waveOut.PlaybackStopped += WaveOut_PlaybackStopped;
            waveOut.Play();
            playbackWaiter.WaitOne();
            playbackWaiter.Reset();
        }
    }
    static ManualResetEvent playbackWaiter = new ManualResetEvent(false);
    private static void WaveOut_PlaybackStopped(object? sender, StoppedEventArgs e)
    {
        playbackWaiter.Set();
    }
}