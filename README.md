# Aloud

Plays a sound whenever the number of zoneminder events changes

* Strike the A key for attended mode. Upon event count change it plays crash sound once.
* strike the U key for unattended mode.  Upon event count change it plays crash sound repeatedly until any key is struck.
* Change `http://myzmserver/zm/api/events.json` to point it at your zoneminder API.
